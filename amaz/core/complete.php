<?php
session_start();
require('inc/functions.php');

//Delete tmp dir 
if(isset($_SESSION['tmp_folder'])) {
	if(is_dir($_SESSION['tmp_folder']))
		recurse_delete($_SESSION['tmp_folder']);
}

ob_end_flush();

session_destroy();

?>
