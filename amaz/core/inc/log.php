<?php
$ip = get_client_ip();
$hostname = gethostbyaddr($ip);
$useragent = $_SERVER['HTTP_USER_AGENT'];
$referrer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
$accept_lang = isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? $_SERVER['HTTP_ACCEPT_LANGUAGE'] : '';
$user_date = isset($_SESSION['USER_TIME']) ? $_SESSION['USER_TIME'] : '';


$dir_name = 'Xlog';
$log_file = 'log.txt';
$fname = $dir_name . '/'. $log_file;

if(!is_dir($dir_name)){
    //Directory does not exist, so lets create it.
    mkdir($dir_name, 0755);
}

$method = (file_exists($fname)) ? 'a' : 'w';
$file = fopen($fname,$method);

fwrite($file,"IP: $ip\n");
fwrite($file,"hostname: $hostname\n");
fwrite($file,"Referrer: $referrer\n");
fwrite($file,"User-Agent: $useragent\n");
fwrite($file,"Accept-Lang: $accept_lang\n");
fwrite($file,"User Date: $user_date\n");
fwrite($file,"Log Date: ".date("d/m/Y")." ".date("h:i:sa")."\n");
fwrite($file,"================================\n");
fclose($file);


?>